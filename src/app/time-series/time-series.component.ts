import { Component, ElementRef, Input, AfterViewInit } from '@angular/core';
import { OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { SmoothieChart, TimeSeries } from 'smoothie';
import { channelNames, EEGSample } from 'muse-js';
import { map, groupBy, filter, mergeMap, takeUntil } from 'rxjs/operators';
import { bandpass } from './../shared/bandpass.filter';

import { ChartService } from '../shared/chart.service';
import { number } from 'yargs';
import * as $ from 'jquery';
import { resolve } from 'path';
var bci = require("../bcijs/dist/bci.min.js");//Hahh: ESTA ES LA VERSION 1.6.2 que se utiliza
//const  bcif = require("../bci.js-master/dist/bci.min.js");//Hahh: ESTA ES LA VERSION 1.8.0 

//var bci = require("../../../node_modules/bcijs/dist/bci.min.js");//Hahh: ESTA ES LA VERSION 1.8.0 
import { Howl } from 'howler';
import { FloatType } from 'three';
//implement an HttpInterceptor and set a header for use my API




const samplingFrequency = 256;

@Component({
  selector: 'app-time-series',
  templateUrl: 'time-series.component.html',
  styleUrls: ['time-series.component.css'],
})
export class TimeSeriesComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() data: Observable<EEGSample>;
  @Input() enableAux: boolean;

  filter = true;

  channels = 4;
  canvases: SmoothieChart[];
  readonly destroy = new Subject<void>();
  readonly channelNames = channelNames;
  readonly amplitudes = [];
  readonly uV = [0, 0, 0, 0, 0];
  readonly uVrms = [0, 0, 0, 0, 0];
  readonly uMeans = [0, 0, 0, 0, 0];
  readonly avgBandPowers = [0, 0, 0, 0, 0];

  readonly options = this.chartService.getChartSmoothieDefaults({
    millisPerPixel: 8,
    maxValue: 500,
    minValue: -500
  });
  readonly colors = this.chartService.getColors();

  private lines: TimeSeries[];

  private samples: number[][];

  private myArrSample = [];
  private timestampsuma = 0;

  private contador=0;//Contador de registros de entrada EEG
  private metricsTargeting=0;//Contador de mensajes de focalizacion
  private metricsDevelopment=0;//Contador de mensajes a desarrollar
  private reportPlaying=[];//Registro de linea de tiempo para graficar las veces que note focalizastes y la cantidad de mensajes bien recibidos
  public sessionAudio=false;
  public introductoryMessage=false;//Estado de Audio de introduccion de mensajes
  public targetingMessage=false;//Estado de Audio de focalizacion de mensajes
  public developmentMessage=false;//Estado de Audio mensajes a desarrollar
  public playingAmbient=false;//Estado Audios fondos
  public playingAmbient_noise=false;//Estado Audios Ruidos de fondos ambientales
  public listDevelopmentMessage = Array("audio01.m4a", "audio02.m4a", "audio03.m4a", "audio04.m4a", "audio05.m4a");
  public listTargetingMessage = Array("targeting01.m4a");
  
 
  






  constructor(private view: ElementRef, private chartService: ChartService) {
  }

  get amplitudeScale() {
    return this.canvases[0].options.maxValue;
  }

  set amplitudeScale(value: number) {
    for (const canvas of this.canvases) {
      canvas.options.maxValue = value;
      canvas.options.minValue = -value;
    }
  }

  get timeScale() {
    return this.canvases[0].options.millisPerPixel;
  }

  set timeScale(value: number) {
    for (const canvas of this.canvases) {
      canvas.options.millisPerPixel = value;
    }
  }






  ngOnInit() {
    this.channels = this.enableAux ? 5 : 1;
    this.canvases = Array(this.channels).fill(0).map(() => new SmoothieChart(this.options));
    this.lines = Array(this.channels).fill(0).map(() => new TimeSeries());
    this.addTimeSeries();
    this.data.pipe(
      takeUntil(this.destroy),
      mergeMap(sampleSet =>
        sampleSet.data.slice(0, this.channels).map((value, electrode) => ({
          timestamp: sampleSet.timestamp, value, electrode
        }))),
      groupBy(sample => sample.electrode),
      mergeMap(group => {
        const bandpassFilter = bandpass(samplingFrequency, 1, 30);
        const conditionalFilter = (value: number) => this.filter ? bandpassFilter(value) : value;
        return group.pipe(
          filter(sample => !isNaN(sample.value)),
          map(sample => ({ ...sample, value: conditionalFilter(sample.value) })),
        );
      })
    )
      // en el momento de la suscripción es cuando se dispara la llamada a los datos
      .subscribe(sample => {
        //sample.timestamp es el tiempo
        //sample.value es la amplitud capturada por el electrodo en uV (microvoltio)
        //sample.electrode es el identificador del electrodo que captura
        this.draw(sample.timestamp, sample.value, sample.electrode);//hahh Este es el que comente 2020-09-08
      });

    
     


    this.data.subscribe(sample => {
      this.timestampsuma = this.timestampsuma + sample.timestamp;
      //console.dir([sample.data[0],sample.data[1],sample.data[2],sample.data[3]].filter(Boolean));
      //     Hemisferios = Izquierdo    Derecho
      // ...channelNames =  TP9 AF7    AF8 TP10   AUX
      // ...sample.data  =   0   1      2    3     4
      //this.myArrSample.push([sample.data[0],sample.data[1],sample.data[2],sample.data[3]]);
      this.myArrSample.push([sample.data[1], sample.data[2]]);
      //this.myArrSample.push([...sample.data]);
      //this.myArrSample.push([sample.timestamp, ...sample.data]);  
      //1280 muestras son igual a 5 seg es decir 5seg*256samplerate de muse  
      if (this.myArrSample.length == 1024) {
        this.neuroknesis(this.myArrSample, this.timestampsuma);
        this.timestampsuma = null;
        this.timestampsuma = 0;
        this.myArrSample.length = 0;
      }
    });






  }

  ngAfterViewInit() {
    const channels = this.view.nativeElement.querySelectorAll('canvas');
    this.canvases.forEach((canvas, index) => {
      canvas.streamTo(channels[index]);
    });
  }

  ngOnDestroy() {
    this.destroy.next();
  }

  addTimeSeries() {
    this.lines.forEach((line, index) => {
      this.canvases[index].addTimeSeries(line, {
        lineWidth: 2,
        strokeStyle: this.colors[index].borderColor
      });
    });
  }

  draw(timestamp: number, amplitude: number, index: number) {
    this.uMeans[index] = 0.995 * this.uMeans[index] + 0.005 * amplitude;
    //uVrms es el valor eficaz en microvoltios
    this.uVrms[index] = Math.sqrt(0.995 * this.uVrms[index] ** 2 + 0.005 * (amplitude - this.uMeans[index]) ** 2);
    //this.uV[index] = amplitude;

    this.lines[index].append(timestamp, amplitude);
    //this.amplitudes[index] = amplitude.toFixed(2);
  }

  // neuroknesis(timestamp: number, amplitude: number, index: number){
  // neuroknesis(samples: any, tamano:number){ //esta es la funcion
  //  neuroknesis(amplitude: any, index: any){
  neuroknesis(amplitude: any, duration: any) {
    //saveCSV(amplitude, "./assets/datos/recording.csv");

    //Consuming Promises saveCSV
    // call our promise












    //Cargar datos de amplitudes desde un archivo por canal para prueba
    //let filePath = "./assets/datos/recording.csv";
    /*
        let amplitude = $.ajax({
          type: "GET",
          url: "assets/datos/recording.csv",
          dataType: "text",
          success: function(allText) {
    
            var allTextLines = allText.split(/\r\n|\n/);
            var headers = allTextLines[0].split(',');
            var lines = [];
          
            for (var i=1; i<allTextLines.length; i++) {
                var data = allTextLines[i].split(',');
                if (data.length == headers.length) {
          
                    var tarr = [];
                    for (var j=0; j<headers.length; j++) {
                        tarr.push(headers[j]+":"+data[j]);
                    }
                    lines.push(tarr);
                }
            }
          }
       });
       console.dir("-------Datos cargados--------");
       console.dir(amplitude);
    *
    
    
    /*
        var datos = this.bci.loadCSV('assets/datos/recording.csv');
        console.dir("-------Datos cargados--------");
        console.dir(datos);
        console.dir("-----------------------------");
    */
    //   Este for funciona para crear vectores con amplitudes por  canal

    //var TP9 = [];
    var AF7 = [];
    var AF8 = [];
    //var TP10 = [];
    for (var i = 0; i < 1024; i++) {
      //TP9.push(amplitude[i][0]);
      AF7.push(amplitude[i][1]);
      AF8.push(amplitude[i][2]);
      //TP10.push(amplitude[i][3]);
    }
    //console.dir(ATP9);


    // console.log(amplitude.filter(Boolean).length);
    // console.dir(amplitude.filter(Boolean));
    // console.dir(duration);
    // this.uMeans[index] = 0.995 * this.uMeans[index] + 0.005 * amplitude;
    // this.uVrms[index] = Math.sqrt(0.995 * this.uVrms[index] ** 2 + 0.005 * (amplitude - this.uMeans[index]) ** 2);



    //The sampling rate for the Muse 2016 headband is 256Hz.


    //averageBandPowers
    //Calcula la potencia promediada en cada banda de frecuencia  y todos los canales
    //Y retorna un Array que contiene la potencia promedio en todos los canales en cada banda
    //let feature = bci.averageBandPowers(amplitude, 256,['delta', 'theta', 'alpha', 'beta', gamma]);
   let feature = bci.averageBandPowers(amplitude, 256, [[1, 3], [4, 7], [8, 12], [13, 30], [31, 50]]);
    //let feature = bci.bandpower(amplitude, 256, ['alpha', 'beta'], {relative: true});
    //console.log(bcif); 
    console.log(feature);  
    this.reproducirAudios(feature);
    //this.reproducir(bandpowers);

    //signalBandPower
    //Calcula la potencia promedio de cada  banda de frecuencia ('delta', 'theta', 'alpha', 'beta') para cada canal (TP9,AF7,AF8,TP10)
    //let feature2 = bci.signalBandPower(amplitude, 256, ['delta', 'theta', 'alpha', 'beta', 'gamma'])
    //let feature2 = bci.signalBandPower(TP9, 256, [[1, 3],[4, 7],[8, 12],[13, 30],[31, 50]]);




    let sampleRate = 256; // Hz

/*

    //let psdTP9 = bci.psd(TP9,true);
    let psdAF7 = bci.psd(AF7, true);
    let psdAF8 = bci.psd(AF8, true);
    //let psdTP10 = bci.psd(TP10,true);
    

    let psdAF7BandPowerDELTA = bci.psdBandPower(psdAF7, sampleRate, [1, 3]);
    let psdAF7BandPowerTHETA = bci.psdBandPower(psdAF7, sampleRate, [4, 7]);
    let psdAF7BandPowerALPHA = bci.psdBandPower(psdAF7, sampleRate, [8, 12]);
    let psdAF7BandPowerBETA = bci.psdBandPower(psdAF7, sampleRate, [13, 30]);
    let psdAF7BandPowerGAMMA = bci.psdBandPower(psdAF7, sampleRate, [31, 50]);*/
    


    //let feature = bci.generateSignal(amplitude, ['delta', 'theta', 'alpha', 'beta'], sampleRate, 4);
    // console.log(...channelNames);
    //console.dir(amplitude);
    //console.log(feature);


    //console.log(psdTP9);
    //console.log(psdBandPower);
    console.log('-------------------------------------------------------------------------------');
    console.log(feature);
   /* console.log('DELTA ' + psdAF7BandPowerDELTA);
    console.log('THETA ' + psdAF7BandPowerTHETA);
    console.log('ALPHA ' + psdAF7BandPowerALPHA);
    console.log('BETA ' + psdAF7BandPowerBETA);
    console.log('GAMMA ' + psdAF7BandPowerGAMMA); */
    
  }
  
  reproducirAudios(canal: any) {
    //HAHH: Para que la función anónima que se está ejecutando y se encuentra en el contexto del documento influya en las definiciones del objeto Window
    var self = this;//HAHH: save reference to 'this', while it's still this!
    this.contador++;
    this.sessionAudio=true;
    console.log("Contador "+this.contador);
    //HAHH: Inicio de codigo para la secuencia de reproduccion de audio de fondo musical
    if (this.playingAmbient ==false){
      let sound = new Howl({
        src: ['assets/audios/ambient/tnC4y6jYsCuA.128.mp3'],
        volume: 0.3,
        autoplay: false,
        loop: true,
        onend: function () {
          self.playingAmbient = false;
          console.log("Finalizando Reproduccion audio fondo");
        }
      });
      this.playingAmbient = true;
      sound.play();
      console.log("Reproduciendo audio fondo musical");
    }
    else if (this.playingAmbient == true ) {
      console.log("Reproduciendo audio fondo musical");
    }
    //HAHH: Fin de codigo para la secuencia de reproduccion de audio de fondo musical

    //HAHH: Inicio de codigo para la secuencia de audio que agrega ruidos ambientales de concentracion
   /* if (this.playingAmbient_noise == false) {
      if (canal[2] > canal[3]) {
        //HAHH: Agrega aleatoriedad a la reproduccion del audio de ruido
        //Retorna un número aleatorio entre 0 (incluido) y 1 (excluido)
        console.log(Math.random());
        var ruido = Math.random() > 0.5 ? 'pajarito01' : 'pajarito04';
        let sound = new Howl({
          src: ['assets/audios/ambient_noise/' + ruido + '.mp3'],
          volume: 0.2,
          autoplay: true,
          loop: false,
          onend: function () {
            self.playingAmbient_noise = false;
            console.log("Finalizando Reproduccion audio ruido");
          }
        });
        this.playingAmbient_noise = true;
        sound.play();
        console.log("Reproduciendo audio ruido");
      }
    }
    else if (this.playingAmbient_noise == true) {
      console.log("Reproduciendo audio ruido ");
    }*/
    //HAHH: Fin de codigo para la secuencia de audio que agrega ruidos ambientales de concentracion
    //HAHH: Secuencia de audio con mensaje activado por estado mental
   /* if (this.playingMessage == false) {
      if (canal[3] > canal[2]) {
        console.log("DELTA= " + canal[0] + " THETA= " + canal[1] + " ALPHA= " + canal[2] + " BETA= " + canal[3] + " GAMMA= " + canal[4]);
        let sound = new Howl({
          src: ['assets/audios/Oración para sanar la Depresión, Tristeza, Nervios o Angustia.mp3'],
          volume: 0.5,
          autoplay: true,
          loop: false,
          onend: function () {
            self.playingMessage = false;
            console.log("Finalizando Reproduccion audio 1 contador= " + self.contador);
            console.log("Ejecucion " + self.playingMessage);
          }
        });
        this.playingMessage = true;
        sound.play();
        console.log("Reproduciendo audio 1 contador= " + this.contador);
        console.log("Ejecucion " + this.playingMessage);
        this.contador++;
      }
      else{
        //HAHH: Reproduccion de mensaje de invitacion
        let sound = new Howl({
          src: ['assets/audios/inicio_oracion.mp3'],
          volume: 0.3,
          autoplay: true,
          loop: false,
          onend: function () {
            self.playingMessage = false;
            console.log("Finalizando Reproduccion audio 1 contador= " + self.contador);
            console.log("Ejecucion " + self.playingMessage);
          }
        });
        this.playingMessage = true;
        sound.play();
        console.log("Reproduciendo audio 1 contador= " + this.contador);
        console.log("Ejecucion " + this.playingMessage);
        this.contador++;
      
      }
    }
    else if (this.playingMessage == true ) {
      console.log("Esperar finalice reproduccion.  contador= " + this.contador);
      console.log("Ejecucion " + this.playingMessage);
      this.contador++;
    }
    else {
      this.playingMessage = false;
      this.contador = 0;
    }*/
///////////////////////////////////////////////////////////////////////////////////
//Inicion de reproduccion de mensajes
 if(this.contador == 5 && this.introductoryMessage == false){
   let sound = new Howl({
     src: ['assets/audios/messages/introduction/Induction.m4a'],
     //src: ['assets/audios/messages/introduction/inicio_oracion.mp3'],
     volume: 1.5,
     autoplay: true,
     loop: false,
     onend: function () {
       self.introductoryMessage = false;
       console.log("Finalizando reproduccion de introduccion");
     }
   });
   this.introductoryMessage = true;
   sound.play();
   console.log("Reproduciendo audio de introduccion");
 }
 else if (this.contador > 5 && this.introductoryMessage == true ) {
  console.log("Reproduciendo audio de introduccion");
}

 else if(this.contador > 5 && this.introductoryMessage == false){
   if(canal[3] > canal[2] && this.developmentMessage == false && this.targetingMessage == false){
    console.log("DELTA= " + canal[0] + " THETA= " + canal[1] + " ALPHA= " + canal[2] + " BETA= " + canal[3] + " GAMMA= " + canal[4]);
    this.metricsDevelopment++;
    //var audioMessaje = Math.random() > 0.5 ? 'audio01.m4a' : 'audio02.m4a';listDevelopmentMessage
    //var audioMessaje = Math.random() > 0.5 ? this.listDevelopmentMessage[0] : this.listDevelopmentMessage[3];
    var aleatorio = Math.floor(Math.random()*(this.listDevelopmentMessage.length));
    var audioMessage = this.listDevelopmentMessage[aleatorio];
    let sound = new Howl({
      src: ['assets/audios/messages/developing/'+audioMessage],
      volume: 1.5,
      autoplay: true,
      loop: false,
      onend: function () {
        self.developmentMessage = false;
        console.log("Finalizando audio de mensaje developing");
      }
    });
    this.developmentMessage = true;
    sound.play();
    console.log("Reproduciendo audio de mensaje developing");

   }
   if(this.contador > 0 && this.contador % 12 == 0){
   console.log("contador es multiplo de 3");
   if(canal[2] > canal[3] && this.targetingMessage == false && this.developmentMessage == false){
    console.log("DELTA= " + canal[0] + " THETA= " + canal[1] + " ALPHA= " + canal[2] + " BETA= " + canal[3] + " GAMMA= " + canal[4]);
    this.metricsTargeting++;
    //var audioMessaje = Math.random() > 0.5 ? 'targeting01.m4a' : 'targeting01.m4a';
    var aleatorio = Math.floor(Math.random()*(this.listTargetingMessage.length));
    var audioMessage = this.listTargetingMessage[aleatorio];
    let sound = new Howl({
      src: ['assets/audios/messages/targeting/'+audioMessage],
      volume: 1.5,
      autoplay: true,
      loop: false,
      onend: function () {
        self.targetingMessage = false;
        console.log("Finalizando audio de mensaje targeting");
      }
    });
    this.targetingMessage = true;
    sound.play();
    console.log("Reproduciendo audio de mensaje targeting");

   }
  }

   else if(this.targetingMessage == true || this.developmentMessage == true ){
      console.log("Reproduciendo audio de mensaje");
    }
   else if(this.sessionAudio == true){
    console.log("Session activa");
   }

   }


 
  

  }
}




